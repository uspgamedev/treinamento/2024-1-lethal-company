class_name Status
extends Node

@export var health: int = 100
@export var attackpow: int = 1
@export var isPlayer: bool = false

signal health_changed(value)
signal attacked

func _physics_process(_delta):
	if health <= 0:
		Death()

func Death():
	if(!isPlayer):
		owner.queue_free()
	else:
		var label = owner.get_child(1)
		label.text = "MORREU"

func _on_hitbox_detector_area_entered(area: Area2D):
	if(area.get_owner() != owner):
		var statusArea = area.get_owner().get_child(0)
		if(!isPlayer and statusArea.isPlayer || isPlayer and !statusArea.isPlayer):
			statusArea.lose_health(attackpow)
			emit_signal("attacked")

func lose_health(dmg: int):
	health -= dmg
	emit_signal("health_changed", health)
	var animat = get_parent().find_child("AnimationBlink")
	animat.play("blink")
