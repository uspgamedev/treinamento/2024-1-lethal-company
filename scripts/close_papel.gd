extends Control

var pause: Control

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("escape"):
		quit_tela()

func _on_button_pressed():
	quit_tela()

func quit_tela():
	queue_free()
	pause.pause_game()
