extends Sprite2D

var in_area: bool = false

@export var label: Label
@export var scene: PackedScene
@export var text_prompt: String
@export var pause: Control

func _process(_delta):
	if Input.is_action_just_pressed("action") and in_area:
		pause.pause_game()
		var node_scene = scene.instantiate()
		node_scene.pause = pause
		get_parent().add_child(node_scene)
		queue_free()
