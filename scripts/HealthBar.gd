extends ProgressBar

@export var char_body: CharacterBody2D
var status: Node

func _ready():
	status = char_body.get_child(0)
	status.health_changed.connect(update)
	update(status.health)

func update(val: int):
	value = val
