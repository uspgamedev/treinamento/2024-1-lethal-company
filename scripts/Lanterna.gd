extends PointLight2D

func _ready():
	shadow_item_cull_mask = shadow_item_cull_mask

func _process(_delta):
	if get_parent().pause_menu.is_paused: return
	look_at(get_global_mouse_position())
	rotate(deg_to_rad(90))
