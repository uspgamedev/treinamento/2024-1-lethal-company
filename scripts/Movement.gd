class_name Movement
extends Node

@onready var char_body: CharacterBody2D = get_parent()
@onready var col: CollisionShape2D = $"../CollisionShape2D"

@export var raycast: RayCast2D
@export var DASH_CD_CONST = 3.0
var dash_cd: float = DASH_CD_CONST

var direction: Vector2
var face_dir: Vector2

@export var ATAQUE_CD_CONST = 1.5
var atk_cd:float = ATAQUE_CD_CONST

func move(delta):
	var dash = Input.is_action_just_pressed("dash")
	if dash and dash_cd == 0:
		dash_cd = DASH_CD_CONST
		char_body.state = char_body.DASH
	
	var ataq = Input.is_action_just_pressed("atirar")
	if ataq and atk_cd == 0 and char_body.has_gun:
		atk_cd = ATAQUE_CD_CONST
		char_body.state = char_body.ATAQUE
	
	if dash_cd > 0:
		dash_cd -= delta
		if dash_cd < 0:
			dash_cd = 0
			
	if atk_cd > 0:
		atk_cd -= delta
		if atk_cd <= 0:
			atk_cd = 0
	
	direction = Input.get_vector("andar_esquerda", "andar_direita", "andar_cima", "andar_baixo")
	if direction:
		face_dir = direction.normalized()
		char_body.velocity = face_dir * char_body.SPEED
	else:
		char_body.velocity.x = move_toward(char_body.velocity.x, 0, char_body.SPEED)
		char_body.velocity.y = move_toward(char_body.velocity.y, 0, char_body.SPEED)
	
	char_body.move_and_slide()
