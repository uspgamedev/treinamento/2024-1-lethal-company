extends Control

var is_paused: bool = false

var game_crosshair = load("res://assets/crosshair.png")

func _process(_delta):
	if Input.is_action_just_pressed("escape") and !is_paused:
		pause_game()
		
func pause_game():
	if is_paused:
		hide()
		Engine.time_scale = 1
		Input.set_custom_mouse_cursor(game_crosshair, 0, Vector2(16,16))
	else:
		show()
		Engine.time_scale = 0
		Input.set_custom_mouse_cursor(null)
	is_paused = !is_paused


func _on_continue_pressed():
	pause_game()


func _on_quit_pressed():
	get_tree().quit()
