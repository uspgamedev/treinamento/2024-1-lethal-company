extends AnimationPlayer

@onready var char_body: CharacterBody2D = get_parent()

@export var move: Node
@export var rayc: RayCast2D

var rd: Vector2 = Vector2.RIGHT.rotated(deg_to_rad(45))
var ld: Vector2 = Vector2.DOWN.rotated(deg_to_rad(45))
var ru: Vector2 = Vector2.UP.rotated(deg_to_rad(45))
var lu: Vector2 = Vector2.LEFT.rotated(deg_to_rad(45))
var dir: Vector2 = rd

var directions = [rd, ld, ru, lu]

func _process(_delta):
	if move.direction:
		dir = move.face_dir
		var n_dir = directions.map(func(nums): return move.direction.dot(nums))
		dir = directions[n_dir.find(n_dir.max())]
	handle_anim()

func handle_anim():
	var anim = ""
	var animar = ["rd", "ld", "ru", "lu"]
	match char_body.state:
		char_body.MOVE:
			if move.direction:
				anim = "walk_" + animar[directions.find(dir)]
			else:
				anim = "idle_" + animar[directions.find(dir)]
		char_body.DASH:
			anim = "dash_" + animar[directions.find(dir)]
		char_body.ATAQUE:
			var rotac = rayc.rotation
			var a_dir = Vector2.RIGHT.rotated(rotac)
			var n_dir = directions.map(func(nums): return a_dir.dot(nums))
			dir = directions[n_dir.find(n_dir.max())]
			anim = "shoot_" + animar[directions.find(dir)]

	play(anim)
