extends Area2D

var in_area: bool = false

var player_body
@export var ammo: Control
@export var ammo_amount: int

func _process(_delta):
	if Input.is_action_just_pressed("action") and in_area:
		player_body.cur_ammo = ammo_amount
		player_body.total_ammo = ammo_amount
		ammo.get_child(0).text = str(ammo_amount)
		ammo.get_child(2).text = str(ammo_amount)
		player_body.has_gun = true
		get_parent().queue_free()

func _on_body_entered(body):
	player_body = body 
	get_child(1).get_child(0).text = "Pick Up?"
	in_area = true

func _on_body_exited(_body):
	in_area = false
	get_child(1).get_child(0).text = ""
