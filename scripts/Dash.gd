extends Node
class_name Dash

@onready var char_body: CharacterBody2D = get_parent()
@onready var move: Node = $"../Movement"

@onready var dash_dir = move.face_dir

@export var DASH_TIME_CONST = 0.2

var dash_time = DASH_TIME_CONST

func _process(_delta):
	if char_body.state == char_body.DASH:
		dash_dir = move.face_dir

func dash(delta):
	if dash_time > 0:
		char_body.velocity = dash_dir * char_body.SPEED * 6
		char_body.move_and_slide()
		dash_time -= delta
		if dash_time <= 0:
			dash_time = DASH_TIME_CONST
			char_body.state = char_body.MOVE
