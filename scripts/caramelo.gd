extends CharacterBody2D

@export var SPEED: float
@onready var animat = $"AnimationPlayer"
@onready var status = $"Status"

var see_player = false

var last_side = "d"

var d: Vector2 = Vector2.DOWN
var u: Vector2 = Vector2.UP
var rd: Vector2 = Vector2.RIGHT.rotated(deg_to_rad(45))
var ld: Vector2 = Vector2.DOWN.rotated(deg_to_rad(45))
var ru: Vector2 = Vector2.UP.rotated(deg_to_rad(45))
var lu: Vector2 = Vector2.LEFT.rotated(deg_to_rad(45))
var dir: Vector2 = d

var directions = [d, u, rd, ld, ru, lu]

var rng = RandomNumberGenerator.new()

var leftDir = rng.randi_range(-1, 0)	
var rightDir = rng.randi_range(0, 1)	
var upDir = rng.randi_range(-1, 0)	
var downDir = rng.randi_range(0, 1)	

var xDir
var yDir

var frames = 0

func _physics_process(_delta):
	
	if !status.isPlayer:
		if see_player == true:
			frames = -1
			var player = get_parent().get_node("player")
			accelerate_towards_point(player)
			move_and_slide()
		else:
			frames+=1
			
			if(frames > 200):
				frames = 0
			
			if(frames == 0):
				leftDir = rng.randi_range(-1, 0)	
				rightDir = rng.randi_range(0, 1)	
				upDir = rng.randi_range(-1, 0)	
				downDir = rng.randi_range(0, 1)	

			xDir = leftDir + rightDir
			yDir = upDir + downDir
			
			var direction = Vector2(xDir,yDir)
			if direction:
				dir = direction
				velocity = direction.normalized() * SPEED
				var n_dir = directions.map(func(nums): return direction.dot(nums))
				dir = directions[n_dir.find(n_dir.max())]
			else:
				velocity.x = move_toward(velocity.x, 0, SPEED)
				velocity.y = move_toward(velocity.y, 0, SPEED)

	handle_anim()

	move_and_slide()

func accelerate_towards_point(point):
	dir = (point.position - position).normalized()
	velocity = dir * SPEED
	var n_dir = directions.map(func(nums): return dir.dot(nums))
	dir = directions[n_dir.find(n_dir.max())]

func handle_anim():
	var anim = ""
	var animar = ["d","u","rd", "ld", "ru", "lu"]
	if velocity.length() > 0:
		anim = "walk_" + animar[directions.find(dir)]
	else:
		if(dir == rd || dir == ld):
			dir = d
		elif(dir == ru || dir == lu):
			dir = u
		anim = "idle_" + animar[directions.find(dir)]

	animat.play(anim)


func _on_player_detector_body_entered(body):
	if body.name == "player":
		see_player = true


func _on_player_detector_body_exited(body):
	if body.name == "player":
		see_player = false
