extends Node
class_name Ataque

@onready var char_body: CharacterBody2D = get_parent()
@onready var move: Node = $"../Movement"

@export var raycast: RayCast2D
@export var ATK_TIME_CONST = 0.2
var atk_time: float = ATK_TIME_CONST
var atk_once: bool = true

var h = ProjectSettings.get_setting("display/window/size/viewport_height") / 2
var w = ProjectSettings.get_setting("display/window/size/viewport_width") / 2
var mid_pos = Vector2(w, h)

func _process(_delta):
	raycast.look_at(raycast.get_global_mouse_position())

func ataque(delta):
	if atk_once and char_body.cur_ammo > 0:
		char_body.cur_ammo = char_body.cur_ammo - 1
		char_body.ammo_label.get_child(0).text = str(char_body.cur_ammo)
		var col = raycast.get_collider()
		if !col: pass
		elif raycast.is_colliding() and col.get_child(0).has_method("lose_health"):
			col.get_child(0).lose_health(owner.get_child(0).attackpow)
		atk_once = false
	atk_timer(delta)

func atk_timer(delta):
	if atk_time > 0:
		atk_time -= delta
		if atk_time < 0:
			atk_time = ATK_TIME_CONST
			atk_once = true
			char_body.state = char_body.MOVE
