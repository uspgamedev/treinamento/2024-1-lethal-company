extends TileMap

func _on_area_boss_body_entered(body):
	if body.name == "player":
		modulate.a = 0.5


func _on_area_boss_body_exited(body):
	if body.name == "player":
		modulate.a = 1
