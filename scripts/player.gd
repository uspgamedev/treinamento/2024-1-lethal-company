extends CharacterBody2D

@export var SPEED = 300.0
@export var cur_ammo: int
@export var total_ammo: int

@export var move: Node
@export var dash: Node
@export var ataque: Node
@export var ammo_label: Control
@export var pause_menu: Node

var has_gun: bool = false

enum {
	MOVE,
	DASH,
	ATAQUE,
}

@export var state = MOVE

func _physics_process(delta):
	if pause_menu.is_paused: return
	match state:
		MOVE:
			move.move(delta)
		DASH:
			dash.dash(delta)
		ATAQUE:
			ataque.ataque(delta)
