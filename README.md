# COLAPSO

### Prólogo

Você é um cientista que foi designado ao cargo de solucionar um desastre biológico, causado por um erro em um experimento secreto.

Durante sua jornada, enfrentará criaturas instigantes e misteriosas, diante de um ambiente sombrio e desconhecido.

Seria, você, capaz de enfrentá-las?


### Como Jogar

#### Controles:

- WASD: movimentação.

- LMB: ataque (requisito: arma).

- Shift: dash.

- E: interação com objetos.

- ESC: menu.


### Considerações

Projeto para o Treinamento do USPGameDev feito em Godot 4.2.1.

Agradecimentos ao tutor da equipe, Luciano, à Oficial Débora e à todos do USPGameDev pela oportunidade.

Participantes: Leonel Morgado, Cauã Santos

Com ajuda de: Pedro Pereira e Arthur Percinoto

Tilemap: https://scrabling.itch.io/pixel-isometric-tiles License(CC BY 4.0) Author: scrabling